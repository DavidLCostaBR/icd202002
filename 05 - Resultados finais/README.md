# Projeto: Análise dos impactos relacionados à distribuição de profissionais da área de saúde para atuarem em casos de COVID 19

## Equipe: MedicalTaskForce-COVID19

## Descrição: Realizar uma análise exploratória considerando a distribuição no número de profissionais de saúde (médicos, enfermeiros, fisioterapeutas) por local (país, região, estado, município), sua correlação com a demanda (número de casos que demandaram internação) e eventual impacto em relação ao número de óbitos por COVID 19. 

OBS: para visualizar os notebooks utilize: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/DavidLCostaBR/icd202002/master)

## Tema: 
Realizar uma análise exploratória considerando a distribuição no número de profissionais de saúde (médicos, enfermeiros, fisioterapeutas) por local (país, região, estado, município), sua correlação com a demanda (número de casos que demandaram internação) e eventual impacto em relação ao número de óbitos por COVID 19. 

## Objetivos: 
- Detectar se a capacidade da região em relação ao número de profissionais na área de saúde impactou nos casos de óbitos por COVID-1; e
- Verificar se existe uma possível “distribuição adequada” de quantidade de “profissionais da saúde / habitante” que evidencie um melhor atendimento aos pacientes.


## Fontes de dados: 
- SIVEP-Gripe;	
- Base do DATASUS, na seção do CNES (Cadastro Nacional de Estabelecimentos de Saúde); 
- RAIS;
- MapasInterativos.


## Desafios:	
- Obtenção de dados atualizados e confiáveis para o estudo; 
- Normalização dos dados (eliminação de discrepâncias) para que os mesmos se aproximem ao máximo da realidade; 
- Identificar o modelo para análise mais apropriado para atingir o objetivo estabelecido; e
- Selecionar e utilizar de forma adequada e eficiente a melhor ferramenta de apoio.

## Membros senior: 

David Lourenço da Costa, 233657, DavidLCostaBR, davidlcosta67@gmail.com, Pós graduação Ciência da Computação, UNICAMP

Leidmar Magnus Festa, 1525280, leidmar, festa@alunos.utfpr.edu.br, Msc Computação Aplicada, UTFPR

Vinicius Araujo, 157500, ViniciusArj, v157500@dac.unicamp.br, Pós Graduação Geologia, UNICAMP



## Membros junior (se houver): 